package com.example.FragmentDemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Jaaksi on 2015/7/9.
 */
public class FragmentInner extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // 声明当前Fragment是否属于Fragment中切换的Fragment
        ((MyActivity)getActivity()).isFromInner = true;

        Log.i(">>","FragmentInner onCreateView");
        return inflater.inflate(R.layout.frg_inner, container, false);
    }

    @Override
    public void onDestroyView() {
        Log.i(">>", "FragmentInner onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(">>", "FragmentInner onDestroy");
        super.onDestroy();
    }


}