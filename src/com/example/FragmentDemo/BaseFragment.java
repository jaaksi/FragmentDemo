package com.example.FragmentDemo;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jaaksi on 2015/7/12.
 */
public class BaseFragment extends Fragment {
    protected Dialog mProgressBar;
    protected Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mProgressBar = new Dialog(getActivity(),R.style.dialog_no_animation);
        View progress = LayoutInflater.from(getActivity()).inflate(R.layout.progressbar, null);
        mProgressBar.setContentView(progress);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        if (mProgressBar.isShowing()) {
            mProgressBar.dismiss();
        }
        super.onDestroyView();
    }
}