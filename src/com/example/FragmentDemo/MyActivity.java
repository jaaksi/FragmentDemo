package com.example.FragmentDemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

/**
 * 三个tab，三个Fragment切换，第一个Fragment切换子Fragment，然后切换到第二个TabFragment
 */
public class MyActivity extends FragmentActivity implements View.OnClickListener {
    private int currIndex;
    private FragmentB fragmentB;
    private FragmentA fragmentA;
    private FragmentC fragmentC;

    public boolean isFromInner = false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        findViewById(R.id.tab_chat).setOnClickListener(this);
        findViewById(R.id.tab_friend).setOnClickListener(this);
        findViewById(R.id.tab_room).setOnClickListener(this);

        fragmentA = new FragmentA();
        fragmentB = new FragmentB();
        fragmentC = new FragmentC();
        replaceFragment(R.id.content, fragmentA);
    }

    public <T extends View> T findView(int id) {
        return (T) super.findViewById(id);
    }

    public void replaceFragment(int id, Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(id, fragment);
//        ft.addToBackStack()
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    public void onChaneTab() {
        if (isFromInner) { // 如果是内部Fragment
            // 被添加到回退栈中的Fragment切换的时候不会被销毁，所以必须将其从回退栈中弹出，弹出回退栈里的Fragment会
            // 把顶部的状态弹出回退堆栈，如果添加回退栈，必须调用这个弹出回退栈
            getSupportFragmentManager().popBackStack();
            // 从管理器的Fragment回退堆栈中把最后放入的由name参数所指定的Fragment对象弹出
//            getSupportFragmentManager().popBackStack("FragmentA",0);
            isFromInner = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_chat:
                if (currIndex != 0) {
                    onChaneTab();
//                    FragmentA fragmentA = new FragmentA();
                    replaceFragment(R.id.content, fragmentA);
                    currIndex = 0;
                }
                break;
            case R.id.tab_friend:
                if (currIndex != 1) {
                    onChaneTab();
//                    FragmentB fragmentB = new FragmentB();
                    replaceFragment(R.id.content, fragmentB);
                    currIndex = 1;
                }

                break;
            case R.id.tab_room:
                if (currIndex != 2) {
                    onChaneTab();
//                    FragmentC fragmentC = new FragmentC();
                    replaceFragment(R.id.content, fragmentC);
                    currIndex = 2;
                }
                break;
        }

    }
}
