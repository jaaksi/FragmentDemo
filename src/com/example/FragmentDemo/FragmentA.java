package com.example.FragmentDemo;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Jaaksi on 2015/7/9.
 */
public class FragmentA extends BaseFragment implements View.OnClickListener {

    private FragmentInner fragmentInner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frg_a, container, false);
        // FIXME Fragment向其所在的Activity传递数据，可以直接将Activity中的数据声明为public，可以自己诶操作
        // 或调Activity中的public方法
        ((MyActivity) getActivity()).isFromInner = false;
        fragmentInner = new FragmentInner();
        Bundle bundle = new Bundle();
        fragmentInner.setArguments(bundle);

        view.findViewById(R.id.btn_change).setOnClickListener(this);

        Log.i(">>","FragmentA onCreateView");
        return view;
    }

    @Override
    public void onDestroyView() {
        Log.i(">>", "FragmentA onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(">>", "FragmentA onDestroy");
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
//        fragmentInner.setTargetFragment(this,0);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        // 自定义Fragment切换动画，0表示没有动画
        ft.setCustomAnimations(R.anim.push_top_in,0);
        ft.replace(R.id.content, fragmentInner);
        // FIXME 将FragmentA添加到回退栈中，切换Fragment时只会调onDestroyView方法，不会调onDestroy方法
//        ft.addToBackStack(null);
        ft.addToBackStack("FragmentA");
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Toast.makeText(getActivity(), "回退栈改变", Toast.LENGTH_SHORT).show();
            }
        });

        ft.commit();
    }
}