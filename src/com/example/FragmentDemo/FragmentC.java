package com.example.FragmentDemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jaaksi on 2015/7/9.
 */
public class FragmentC extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MyActivity)getActivity()).isFromInner = false;
        return inflater.inflate(R.layout.frg_c, container, false);
    }
}